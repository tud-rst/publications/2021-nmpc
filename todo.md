TODO
====


Offen (nach Priorität)
----------------------


   
Erledigt (chronologisch)
------------------------

1. Shape functions in approximation (compact support on [Gamma_s, Gamma_l] below (9)?
   Stefan: Ist so i.O.	

2. Matrizen/Vektoren im appendix, mein Vorschlag:
	- q2 -> dz,dz
	- P3 -> dz, -
	- q3 -> dz, -
	Stefan: ?
		
3. Gleichung (2): Könnte man nicht auch \partial_r^2 wie auch beim z-Anteil schreiben?
	Tom, Stefan: Nein -> erledigt.

4. Kurz darauf wird von der theta-Notation zur T-Notation bei der Temperatur gewechselt, was jedoch nicht wie bspw. in Gleichung (5) vorher eingeführt wird. Evtl. verbesserungswürdig für den Lesefluss.
	Stefan: In Abs. 2.1 darf keine Theta auftreten -> gefixt.

5. Abschnitt 4.1, Abs. 1: Bei der Ungleichung mit den Gewichten passen die
   Dimensionen nicht: Skalar  \le Vektor \le Skalar

6. Fig. 3: Fände ich in der rechten Spalte angenehmer.
   Stefan: Anordnung ist automatisch, Feinjustierung als allerletztes, done.

7. Abschnitt 5.2, vorletzter Abs., letzter Satz mit "Thus, ...": Fehlt hier irgendwo ein Verb?
   Stefan: Da fehlte ein Formelzeichen. -> gefixt.

8. Abschnitt 5.4, Abs. 1 sowie Tab. C.1 und Fußnote Tab. C.3: Die Approximationsordnung N taucht mit 4 verschiedenen Indizes auf: N_d, N_fem, N_sim, N_des.
	Stefan: Auftreten von N_d gefixt. 
	Patrick: Mir ist der Unterschied zwischen N_fem und N_sim noch nicht klar.
	Stefan: N_fem und N_sim sind das gleiche ....

9. Abschnitt 5.4, Abs. 2: Es wird bzgl. des rel. Fehlers von 10 % auf den unteren Teil von Tab. C.1 verwiesen. Fehlt dort was?
	Stefan: Parameter ergänzt.

10. Auf die Abkürzungsdefinition "MPC" könnte man evtl. verzichten (s. Abschnitt 6, letzter Satz).
	Stefan: Akronym gestrichen.

11. Wäre schöner, wenn die Überschrift "Appendix C" und die zugehörigen
	Tabellen zusammenfänden. Habe allerdings noch keine Idee wie am besten.
	Stefan: Siehe Punkt 6, Tabellen jetzt ans Ende gebannt.

12. Tab. C.2: Sind die unterschiedlichen Werte für T_m bei w_s und w_l so richtig?
	Stefan: Ja. Da sind .15°K sicherheit dabei ;-)

13. Tab. C.3: Ist die Notation "blockdiag" so gewollt?
	Stefan: Ja.

14. Die Überschriften der Unterabschnitte sind inkonsistent bzgl. der Groß-/Kleinschreibung:
	Manchmal ist nur das erste Wort, manchmal sind auch alle nachfolgenden Worte großgeschrieben.
	Stefan: gefixed.

15. Mehr Kontext in Verifikations-Abschnitt
	Stefan: Done.

16. Gleichung (17) Vorzeichen prüfen
	Stefan: Erledigt.

17. a_i b_i c_i umbenennen
	Stefan: Erledigt.

18. psi_v Def. ergänzen
	Stefan: Erledigt.

19. Readme aktualisieren
	Patrick: Werde eine Vorlage erstellen, die dann ergänzt werden kann.
	
20. Casadi und Casadi MPC tools lizensen checken
	Casadi: GNU Lesser General Public License v3.0 (https://github.com/casadi/casadi/blob/master/LICENSE.txt)
	MPCTools: GNU General Public License Version 3 (https://bitbucket.org/rawlings-group/mpc-tools-casadi/src/master/COPYING.txt)
	
21. Berechnungen in model_1d.py an Darstellung im paper anpassen
    Stefan: Erledigt.
    
22. Achsenbereich in Fig 4 und 5 angleichen für bessere Vergelichbarkeit
    Stefan: Erledigt.
    
23. Tabellen in App. C in den Griff kriegen (angepasste Version lieferte 9 Seiten)
    Stefan: Erledigt, ob das die kommenden Änderungen überlebt ist aber fraglich...
    
24. Reviewer 4: It would just be interesting to know if it is planned to
    deploy the results on a real world physical system.
    Stefan: Ausblick angepasst.

25. Reviewer 5: Added remarks on dislocations and impurities to outlook