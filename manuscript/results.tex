\section{Results}
\label{sec:results}
\FloatBarrier  % no fancy graphs before the actual result section

The control design of the previous section will now be evaluated for
different scenarios.
To do so
the \gls{fem} approximation from
Section \ref{sec:fem_model} will serve as a reference model for the control
design, after its convergence has been discussed.
Next, the control performance for an ideal case and two more realistic cases
will be presented.
The corresponding parameters are given in Table \ref{tab:sim_params}.


\subsection{Model verification}
\label{ssec:results_verification}

To verify the convergence of the approximated model \eqref{eq:approx_dynamics}, 
a benchmark scenario (cf.~Appendix \ref{app:verification})
is simulated, for which an analytic expression -- the Neumann solution -- exists.
For the simulations, the approximation order $N$ of the series expansion
\eqref{eq:gf_series_trunc} is iteratively increased, yielding 
an approximation error that is supposed to decrease.
As Figure \ref{fig:fem_validation} shows, the errors for $\zi$
decay for growing $N$ as the heat transport effects can be described
better by the model. 
For $N \ge 10$, the error falls below \SI{1}{\milli\metre} and is below
\SI{100}{\micro\metre} for $N=64$.

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{../include/verification.pdf}
	\caption{
		Deviation of the phase boundary $\Delta\gamma(t) = \gamma^N\!(t) - \zir$ 
		between the simulated trajectories of the phase boundary 
		and the analytic reference solution $\zir$ in original coordinates
		for different approximation orders.% $N$.
	}
	\label{fig:fem_validation}
\end{figure}


\subsection{Control setup \& implementation}
\label{ssec:results_setup}

For the remaining simulations, the following scenario is assumed: 
Initially, the phase boundary is resting ($\dot \gamma(0) = \SI{0}{\metre\per\second}$) at
$\gamma(0) = \SI{0,02}{\metre}$.
Furthermore, as a result of the previous melting step a gradient of
$\partial_z\ts(\gamma(0), 0) = \SI{10}{\kelvin\per\centi\metre}$ has been
established throughout the crystal.
Now, the control objective is to grow a valid crystal within $t_f= \SI{70}{\hour}$.
Thus, $\zi$ has to be transferred to \SI{0,38}{\metre} 
while adhering to the objectives for the growth speed $\vi$,
the interface gradient on the liquid side $\partial_z T(\zi, t)$,
and the ratio $\vg$.

Making a trade-off between accuracy and problem size, a model with
$N_{\mathrm{cont}}=10$ nodes per domain is used for the control design.
Furthermore, a horizon length $\hor=20$ with a discretisation step of
$\Delta t= \SI{30}{\min}$ is chosen, 
yielding a planning domain of $\frac{1}{7}t_{f} = \SI{10}{\hour}$
and a final problem with \num{2140} variables to be solved by
\texttt{IPOPT} \citep{Waechter2006}.

\subsection{Reference scenario}

As a baseline, a setup is chosen where the design model used by the controller
exactly matches the simulated system which will be referred to as scenario \textit{A}. 
Figure \ref{fig:profile_a} shows that in this case, a crystal of the desired length
can be grown within the given time frame.
As can be seen from the associated control inputs in Figure \ref{fig:inputs}, the controller
primarily exploits the bottom heater for this purpose.
Finally, Figure \ref{fig:objectives} shows that
all secondary objectives are fulfilled with an initial exception for the liquid gradient.


\subsection{Model errors}

In reality, the design model used for the controller differs from the plant.
Hence, in order to investigate the influence of model errors on the controller performance,
an approximation order $N_{\mathrm{sim}}=64$ is used for the simulation model
in scenario \textit{B} while the controller model stays at $N_{\mathrm{cont}}=10$.
In this case the crystal also manages to reach its desired length
but the input trajectories
in Figure \ref{fig:inputs} show that the controller is struggling and relying
more on the lower and middle jacket heater.
However, this time the gradient objective in Figure \ref{fig:objectives} is violated 
at \SI{27}{\hour}. 

Finally, the performance in the presence of parameter uncertainties is
investigated.  For this purpose the physical simulation parameters from the
previous scenario were altered to reflect a relative error of
$\SI{10}{\percent}$ (cf.~lower part of Table \ref{tab:sim_params}).
This is referred to as scenario \textit{C}.
As can be seen in Figure \ref{fig:profile_c}, the controller has to increase
the melt temperature within the growth process.
Furthermore, the input trajectories in Figure \ref{fig:inputs} changed drastically with the most
significant difference being the maximum usage of the top heater.
Lastly, Figure \ref{fig:objectives} shows that the objectives can still be mostly satisfied,
with the apparent exception of the liquid gradient falling below the desired minimum between
\SI{10}{\hour} and \SI{30}{\hour}. 
This issue arises from the fact that the controller fails to lower the solid gradient and
thus makes a trade-off to reach a feasible growth velocity by means of \eqref{eq:stefan_cond}.

%\begin{figure*}
	%\centering	
	%\subfigure[][Case A]{
		%\includegraphics[width=.48\linewidth]{../include/temp_profile_0.pdf}
		%\label{fig:profile_a}
	%}%
	%\subfigure[][Case A]{
		%\includegraphics[width=.48\linewidth]{../include/temp_profile_2.pdf}
		%\label{fig:profile_c}
	%}
	%\caption{%
		%Resulting temperature profiles $\torigz$ with the corresponding phase boundary $\gamma(t)$ 
		%(blue, solid) and its reference (green, dashed) for different simulation cases.
	%}
	%\label{fig:profiles}
%\end{figure*}
\begin{figure}
	\centering	
	\includegraphics[width=\linewidth]{../include/temp_profile_0.pdf}
	\caption{%
		Resulting temperature profile $\torigz$ with the corresponding phase boundary $\gamma(t)$ 
		(blue, solid) and its reference (green, dashed) for simulation case \textit{A}.
	}
	\label{fig:profile_a}
\end{figure}
\begin{figure}
	\centering	
	\includegraphics[width=\linewidth]{../include/temp_profile_2.pdf}
	\caption{%
		Resulting temperature profile $\torigz$ with the corresponding phase boundary $\gamma(t)$ 
		(blue, solid) and its reference (green, dashed) for simulation case \textit{C}.
	}
	\label{fig:profile_c}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../include/input_trajectories.pdf}
	%\resizebox {\linewidth} {!} {
		%\input{../include/input_trajectories.pgf}
	%}
	\caption{
		Input trajectories with respective boundaries 
		for ideal (A), real (B) and erroneous case (C).
	}
	\label{fig:inputs}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../include/objective_trajectories.pdf}
	%\resizebox {\linewidth} {!} {
		%\input{../include/objective_trajectories.pgf}
	%}
	\caption{
		Process objective trajectories with desired minima and maxima (cf.~Table \ref{tab:objectives})
		for the ideal (A), real (B) and erroneous case (C).
	}
	\label{fig:objectives}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "root"
%%% End:
