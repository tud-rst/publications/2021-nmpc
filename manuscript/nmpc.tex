\section{Nonlinear model predictive control}
\label{sec:nmpc}


\subsection{Constraints \& costs}
\label{ssc:costs}


To comply with the modelling assumptions, a set of constraints has to be
formulated for the model to remain physically plausible.
In detail, for the weights $\wbvs$ in the crystal
it is necessary to keep the temperatures below
the melting temperature $\tm$ while the weights $\wbvl$ in the melt must stay
above.
Similarly, to limit the expansion-induced shear stress in the crystal,
a lower bound for the solid weights is needed while an upper bound for
the weights in the liquid domain saves the melt from overheating.
These requirements are met by enforcing $\wbl \le \wbi{i} \le \wbu$
for each phase with the respective lower and upper bounds.
%given in Table \ref{tab:restrictions}.
Furthermore, due to the limited physical extent of the system
the condition $\zbs < \zi < \zbl$ must be met,
yielding the state constraint vectors
$\xbl^T \coloneqq \begin{pmatrix}
	\wvbls^T &\wvbll^T &\zbs
\end{pmatrix}$ as well as $\xbu^T \coloneqq \begin{pmatrix}
	\wvbus^T &\wvbul^T &\zbl
\end{pmatrix}$.
Regarding the inputs, the heat flows applied by the heaters around the
crucible and their time derivatives are limited.
This fact is expressed via $\ubl \le \uo \le \ubu$ and
$\bs{\nu}_\mathrm{L} \le \dot{\bs{u}}(t) \le \bs{\nu}_\mathrm{U}$
in which the components of the constraint vectors adhere to the ordering in 
\eqref{eq:input_vector}.
For the concrete values of all constraints please refer to Table
\ref{tab:restrictions}.
%$\ubl \le \uo \le \ubu$
%yielding $\uhbl \le \nu(t) \le \uhbu$ as well as $\duhbl \le \dot\nu(t) \le \duhbu$
%for  $\nu(t) \in \left\{\inps, \inpl \right\}$.
%For the heat flow on the mantle surface $\uhblm \le \nu_m(t) \le \uhbum$ and $\duhblm \le \dot\nu_m(t) \le \duhbum$
%for $\nu_m(t) \in \left\{ \inpm{1}, \inpm{2}, \inpm{3} \right\}$ applies.
%Moreover, the forced convection in the melt through the magnetic fields is limited
%which results in $\ucbl \le \inpbc \le \ucbu$ with no restriction on its rate of change.
%This yields the constraint vectors
%$\ubl, \ubu, \dot{\bs{u}}_\mathrm{L}, \dot{\bs{u}}_\mathrm{U}$.
%The numeric values of the introduced constraints on the system's states and inputs are shown in
%Table \ref{tab:restrictions}. 

Regarding the main objective of growing a crystal, the base costs
are chosen as the quadratic form
\begin{equation}
	l_0(\bs{x}(t), \bs{u}(t)) \coloneqq 
		\tilde{\bs{x}}(t)^T \bs{Q}\tilde{\bs{x}}(t) 
		+ \uo^T \bs{R} \uo
	\label{eq:main_objective}
\end{equation}
where $\tilde{\bs{x}}(t) \coloneqq \bs{x}(t) - \bs{x}_{\mathrm{ref}}$ 
denotes the deviation from the reference state
$\bs{x}_{\mathrm{ref}}^T \coloneqq \begin{pmatrix}
	\wbvrs^T &\wbvrl^T &\gamma_{\mathrm{ref}}
\end{pmatrix}$\footnote{%
	In detail, the used reference state is given as an equilibrium profile,
	computed for a constant growth rate $\dot\gamma_{\mathrm{ref}}$ and a desired
	solid gradient $\gsr \coloneqq \partial_z T_{\mathrm{s,ref}}(\zi)$.
}
and $\bs{Q}$ as well as $\bs{R}$ denote the weight matrices.

However, in order to obtain a high quality crystal 
additional process targets must be met.
%parameter constraints from \ref{tab:restrictions} 
To avoid dislocations in the crystal lattice, the growth velocity must
stay positive but below a certain level $\dot{\gamma}_{\mathrm{max}}$.
In addition, the temperature gradient on the liquid side of the phase 
boundary $\gl \coloneqq \partial_z T_{\mathrm{l}}^N\!(\zi, t)$
must stay above $\glmin$
to prevent spontaneous crystallisation in the 
melt and the formation of twins.
Furthermore, in reality, if the growth rate is too high and the latent
heat cannot be transported away from the centre fast enough,
the interface will develop a concave shape. This has to be
avoided because it will lead to an unusable crystal. 
However, as the interface deflection cannot be accounted for in the employed
one-dimensional model, the ratio of growth rate and the gradient on the solid
side of the interface $\vg \coloneqq \vi/\partial_z\ts(\zi, t)$
\citep{Vanhellemont2013} is used instead and must stay below $\vgmax$.
Finally, as the objectives are formulated in the original distributed
temperature $\torigz$ but only the approximated state $\bs{x}(t)$ is available
at runtime, their approximations are derived by the means of
\eqref{eq:gf_series_fixed}, yielding $\glx$ and $\vgx$, respectively.

Next, instead of explicitly enforcing these process targets using constraints,
they are accounted for with the terms:
\begingroup
\allowdisplaybreaks
\begin{align}
	\label{eq:side_objective_1}
	\begin{split}
		\lvi &\coloneqq 2 
		+ \erf \Bigl(-\slope_1 \bigl(\vi - \vimin \bigr)+\shift\Bigr)
		\\&\qquad 
		+ \erf\Bigl(\slope_1 \bigl(\vi - \vimax \bigr)-\shift\Bigr)
	\end{split} &\\
	\label{eq:side_objective_2}
	\lgr &\coloneqq 1 +  \erf\Bigl(
		-\slope_2 \bigl(\glx - \glmin\bigr)
		+\shift
	\Bigr)\\
	%\intertext{and}
	\label{eq:side_objective_3}
	\lvg &\coloneqq 1 + \erf\Bigl(\slope_3 \bigl(\vgx - \vgmax\bigr) -\shift\Bigr).
\end{align}
\endgroup
Herein, $\erf(x) \coloneqq \frac{2}{\sqrt{\pi}} \int_{0}^{x} e^{-\tau^2} d\tau$
is the error function, with argument shift $\shift$ and scalings $\slope_i$.
%Each error function is shifted by $\shift$, so that the contributed cost is low
%when the desired restriction is met.

Finally, taking the weighted sum of \eqref{eq:main_objective} to 
\eqref{eq:side_objective_3}, the stage cost function
\begin{equation}
	l(\bs{x}, \bs{u}) \coloneqq l_0(\bs{x}, \bs{u}) + p \bigl(\lvi+ \lgr + \lvg\bigr)
	\label{eq:stage_cost}
\end{equation}
%with the quality parameter cost terms $\lvi, \lgr$
%and $ \lvg$ 
with the weighting factor $p$ between primary and secondary objectives
is obtained.
All objective parameters can be found in Table \ref{tab:objectives}.


\subsection{Problem statement}
\label{ssc:problem}

The spatially discretised nonlinear state space system (\ref{eq:nonlin_ss})
obtained in Section \ref{sec:fem_model} is continuous in time and therefore
needs to be reformulated as the discrete-time system
\begin{equation}
	\bs{x}_{n+1} = \bs{f}_\mathrm{d}(\bs{x}_n, \bs{u}_n)
	\label{eq:nonlin_ssd}
\end{equation}
where $\bs{x}_n = \bs{x}(n \Delta t)$ with discretisation step $n\in\mathbb{N}$ and
sampling time $\Delta t \in \mathbb{R}^+$ in order to apply \gls{nmpc}.
Both is done with the help of \texttt{MPCTools} \citep{mpctools}, an interface for
the \texttt{CasADi} framework \citep{Andersson2019}. 
Specifically, the time discretisation is carried out using
fourth-order orthogonal collocation.
With this in mind, the optimal control problem to be consecutively solved
on a horizon $\hor \in \mathbb{N}_{> 1}$ can be stated as finding the optimal control
\begin{subequations}
	\begin{align}
		%\argmin_{\bs{U}_{\hor}}\; J_{\hor}(\bs{x}_0, \bs{U}_\hor) &= 
		\bs{U}_{\hor}^* = \argmin_{\bs{U}_{\hor}} &\sum_{n=0}^{\hor-1} l_\mathrm{d}(\bs{x}_u(n, \bs{x}_0), \bs{u}_n)\\
	\intertext{subject to}
		\bs{x}_u(0, \bs{x}_0) &= \bs{x}_0\\
		\bs{x}_u(n+1, \bs{x}_0) 
		&= \bs{f}_\mathrm{d}(\bs{x}_u(n,\bs{x}_0), \bs{u}_n)\\
		\xbl &\leq \bs{x}_u(n, \bs{x}_0) \leq \xbu\\
		\ubl &\leq \bs{u}_n \leq \ubu\\
		\bs{\nu}_\mathrm{L} &\leq (\bs{u}_{n+1} - \bs{u}_n) / \Delta t \leq \bs{\nu}_\mathrm{U}
	\end{align}%
	\label{eq:OCP}%
\end{subequations}
$\forall n \in [0,\hor) \subset \mathbb{N}_0$
where $\bs{x}_u(n, \bs{x}_0)$ denotes the predicted system trajectory that
results from applying the input sequence 
$\bs{U}_\hor \coloneqq \{\bs{u}_0, \bs{u}_1, ..., \bs{u}_{\hor-1}\}$ 
starting from $\bs{x}_0$. This input sequence is to be
optimized by minimizing the sum of the discrete scalar stage cost
$l_\mathrm{d}(\bs{x}_u(n, \bs{x}_0), \bs{u}_n)$ obtained from the continuous stage cost
\eqref{eq:stage_cost} via quadrature.
Since there are no equality constraints or
costs associated with the terminal state $\bs{x}_{\hor}$, this
particular \gls{nmpc} approach can be referred to as one without stabilizing terminal
conditions \citep[cf.][]{Gruene2017}.

