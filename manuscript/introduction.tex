\section{Introduction}

\subsection{Motivation}

Unlike the more common \gls{cz} process \citep{Friedrich2015} in which crystals are
grown from the melt in an upside-down configuration, the \gls{vgf} technique uses
an upward growth direction which is more beneficial in terms of the heat flow
in the crystal.  Therefore, it can be used for the bulk production of high
quality compound semiconductor single crystals \citep{JURISCH2005283} like
\gls{gaas} or Indium-Phosphide that are hard to grow in a \gls{cz} setup.

\begin{figure}
	\centering
	\def\svgwidth{.8\linewidth}
	%\input{../include/kronos_overview_text_asym.pdf_tex}
	\input{../include/kronos_overview_text_asym_flows.pdf_tex}
	\caption{Sketch of a \gls{vgf} crystal growth furnace and associated
	magnet and flow fields.}
	\label{fig:PlantSketch}
\end{figure}%

In detail, the process sketched in Figure \ref{fig:PlantSketch} works as
follows: A seed crystal is placed at the bottom of a rotationally symmetric
crucible which is later filled with solid semiconductor chunks.  After all
material (up to the seed) in the crucible is molten, a vertical temperature
gradient is moved through the plant such that a single crystal grows from the
bottom to the top.
While doing so, the crystallisation interface must be kept in a flat or slightly
convex shape to avoid the creation of stacking faults whose occurrence can lead
from degraded properties up to twinning and thus unusable crystals.
Typically, the process inputs are the heat flows into the system, applied by the
the heaters surrounding the crucible.
However, in a CGD $\textrm{Kronos}^\copyright$ plant \citep{FR2014}, 
the three jacket heaters are also designed as Heater-Magnet-Modules, allowing
to apply a magnetic field that can drive a flow field which in turn increases
heat transport by means of forced convection.

The underlying physical process can be described by two \glspl{fbp} for crystal
and melt, coupled by the dynamics of the crystal-melt interface that form a
nonlinear \gls{tpsp} \citep{Crank84}. 
This interesting type of system is already broadly discussed in the framework
of \glspl{dps} with works including Lyapunov-based \citep{petrus2010},
flatness-based \citep{Ecklebe2021}, backstepping-based \citep{Koga2019a},
\citep{Ecklebe2020} and optimal \citep{Kang1995, Hinze2009} control
designs.  
However, at this point, no designs are available that adhere to the
plant limitations as there are for the \gls{cz} process 
\citep{ABDOLLAHI2014, Rahmanpour2016},
or that incorporate the melt convection as \acrlong{dof}.
This motivates the present work.


\subsection{Objective \& structure}

The main objective of this contribution is to present a \gls{nmpc} design for
the \gls{vgf} growth process.
To do so, Section \ref{sec:modelling} derives a simplified one-dimensional
distributed parameter model of the process that contains the most important
effects.
Next, this model is approximated by using the \gls{fem} in Section
\ref{sec:fem_model}.
Based on this system, the main Section \ref{sec:nmpc} formulates a nonlinear
control problem that expresses the system limitations and process requirements
and describes the complete control setup.
Afterwards, the result Section \ref{sec:results} discusses the verification of the
\gls{fem} model, the control performance as well as the effects of parameter
uncertainties and disturbances.
Finally, a summary and an outlook to further work is given.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "root"
%%% End:
