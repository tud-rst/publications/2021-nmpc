"""
FEM Approximation of a one dimensional two-phase stefan problem as it
occurs in the vertical gradient freeze process.
"""
import logging
from typing import Union, Callable

import casadi as ca
import numpy as np
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
from sympy import Symbol
from matplotlib import pyplot as plt

import pyinduct as pi
import pyinduct.core as cr
import mpctoolbox as mt

from plotting.plot_helper import colourplot


logger = logging.getLogger(__name__)


class Material:
    """
    Helper class for physical material properties.

    Args:
        name (str): Material to grow, defaults to ``GaAs``.
        uncertainty_factor (float): Factor for the material properties to
            simulate parameter uncertainties.
    """

    def __init__(self, name: str, uncertainty_factor: float = 1.0):
        if name == "GaAs":
            self.c = np.array([423.59, 434])
            self.k = np.array([7.1729, 17.8])
            self.rho = np.array([5.1712e+03, 5.7024e+03])
            self.L = 668.5e3
            self.T_m = 1511.15
            self.rho_m = 5.7131e+03
        elif name == "GaAs_const_rho":
            self.c = np.array([423.59, 434])
            self.k = np.array([7.1729, 17.8])
            self.rho = np.array([5.7131e+03, 5.7131e+03])
            self.L = 668.5e3
            self.T_m = 1511.15
            self.rho_m = 5.7131e+03
        elif name == "H2O":
            self.c = np.array([1.7, 4.1868])
            self.k = np.array([2.66, 0.6])
            self.L = 333400
            self.T_m = 273.15
            self.rho_m = 1000
            self.rho = np.array([1000, 1000])  # TODO
        else:
            raise ValueError("Unknown Material '{}'".format(name))

        # apply parameter uncertainties
        self.ucf = uncertainty_factor
        self.c *= self.ucf
        self.k *= self.ucf
        self.L *= self.ucf
        self.rho *= self.ucf
        self.rho_m *= self.ucf

        # compute diffusivities
        self.alpha = np.asarray([self.k[p] / (self.c[p] * self.rho[p])
                                 for p in range(2)])

        # compute ?
        self.v = np.sqrt(self.alpha[0] / self.alpha[1])

    def get_params(self):
        return self.c, self.alpha, self.k, self.L, self.T_m, self.rho_m, self.v


class FEModel(mt.System):
    """
    FEM Approximation of a 1d two-phase stefan problem

    Args:
        n_fem (int):        Number of nodes for each domain.
        dom_len (float):    Length of simulation area.
        material (Material): Material to grow.
        xs (np.ndarray): State scaling vector to apply for better numeric 
            convergance.
        us (np.ndarray): Input scaling vector to apply for better numeric 
            convergance.

    Keyword Args:
        r_ref (float): Maximum radius ``R`` the 2d model.
        u_dim (int): Dimension of the system input, can be one of the following:
            * 2: Only top and bottom heaters
            * 5: Top, bottom  and 3 jacket heaters
            * 6: Top, bottom and jacket heaters as well as melt convection.
            Defaults to 6.
        sim_mode (bool): If True, `get_rhs()` output is numpy expression,
            otherwise output is an casadi expression for. Defaults to False.
        heater_defect (bool): Simulate a middle heater defect occurs after 35
            hours. Defaults to False.
        heater_issues (bool): Simulate a constant error of (100 W/m^2) to bottom
            jacket heater and only 90% of heating/cooling effect of top mantle
            heater. Defaults to False.
    """
    def __init__(self, n_fem: int, dom_len: float, material: Material,
                 xs: np.ndarray, us: np.ndarray,
                 r_ref: float = 0.1, u_dim: int = 6,
                 sim_mode: bool = False,
                 heater_issues: bool = False, heater_defect: bool = False):
        # physical constants
        self.m = material
        self.len = dom_len
        self.orig_bounds = (0, self.len)
        self.deltas = (-1, 1)

        # geometric parameters
        self.n_fem = n_fem
        self.R = r_ref

        self.sim_mode = sim_mode

        # Heater calibration errors
        self.heater_issues = heater_issues
        self.calibration_error = 0
        # factors of 1 mean no error
        self.factor_through_beta_error = 1
        if self.heater_issues:
            self.calibration_error = 100
            self.factor_through_beta_error = 0.9

        self.heater_defect = heater_defect
        self.factor_mid_heater = 1
        if heater_defect:
            self.factor_mid_heater = 0

        # init fields
        self.dTdz_s_data_original = []
        self.dTdz_l_data_original = []
        self.gamma_dot_data = []
        self.u_real_dict = {}

        # dimension of system state
        self.x_dim = self.get_x_dim(n_fem)

        # dimension of system input
        if u_dim not in [2, 5, 6]:
            err = ValueError("Unsupported input dimension provided")
            logger.exception(err)
            raise err
        self.u_dim = u_dim

        self._build_expressions(dom_len, n_fem)

        # finally, build a casadi system
        x_sym = ca.SX.sym('x', self.x_dim)
        u_sym = ca.SX.sym('u', self.u_dim)
        x_dot_p = self._build_rhs(x_sym, u_sym)
        sym_dict = dict(x=x_sym, u=u_sym)
        scale_dict = dict(x=xs, u=us)
        super().__init__(x_dot_p, sym_dict, scale_dict=scale_dict)

    @staticmethod
    def get_x_dim(n_fem):
        return 2 * n_fem - 1

    def _build_expressions(self, dom_len: float, n_fem: int):
        """ build symbolic expressions """
        z = Symbol('z')
        identity_func = z
        sqr_func = z ** 2
        identity_func_dz = identity_func.diff(z)
        sqr_func_dz = sqr_func.diff(z)

        lambdified_one = pi.LambdifiedSympyExpression([1, 0], z, (0, 1))
        lambdified_identity = pi.LambdifiedSympyExpression(
            [identity_func, identity_func_dz],
            z,
            (0, 1)
        )
        lambdified_sqr = pi.LambdifiedSympyExpression([sqr_func, sqr_func_dz],
                                                      z,
                                                      (0, 1))

        one_func = pi.Function(lambdified_one, (0, 1), (0, 1))
        z_func = pi.Function(lambdified_identity, (0, 1), (0, 1))
        z_sqr_func = pi.Function(lambdified_sqr, (0, 1), (0, 1))

        self.nodes = pi.Domain((0, 1), num=n_fem)
        self.fem_base = pi.LagrangeFirstOrder.cure_interval(self.nodes)
        self.one = pi.Base(one_func)

        # split the basis
        self.phi_j = pi.Base(self.fem_base[:-1])
        self.phi_N = pi.Base(self.fem_base[-1])

        # compute variants
        self.z_phi_j = self.phi_j.scale(z_func)
        self.phi_j_dz = self.phi_j.derive(1)
        self.z_phi_j_dz = self.phi_j_dz.scale(z_func)

        self.phi_N_dz = self.phi_N.derive(1)
        self.z_phi_N_dz = self.phi_N_dz.scale(z_func)

        self.phi_j_0 = self.eval_funcs(self.phi_j, 0)
        self.phi_j_dz_1 = self.eval_funcs(self.phi_j_dz, 1)
        self.phi_N_dz_1 = self.eval_funcs(self.phi_N_dz, 1)

        # Parameter for geometric weight functions of the 3 jacket heaters
        self.b_mantle = [-9 / (4 * dom_len ** 2),
                         -9 / (4 * dom_len ** 2),
                         -9 / (4 * dom_len ** 2)]
        self.c_mantle = [3 / (2 * dom_len),
                         9 / (4 * dom_len),
                         3 / dom_len]
        self.d_mantle = [3 / 4, 7 / 16, 0]

        # time independent part of the psi_bar vector
        self.rho_bar = pi.Base([one_func, z_func, z_sqr_func])

        # projected vortex characteristic (different choices are possible)
        self.psi_v_bar = one_func
        # self.psi_v_bar = z_func

        # pyinduct by convection computes the transposed scalar product matrix
        self.P0 = cr.calculate_scalar_product_matrix(self.phi_j, self.phi_j).T
        self.P0_inv = np.linalg.inv(self.P0)

        p1 = cr.calculate_scalar_product_matrix(self.z_phi_j_dz,
                                                self.phi_j).T
        self.P1 = self.P0_inv @ p1

        p2 = cr.calculate_scalar_product_matrix(self.phi_j_dz,
                                                self.phi_j_dz).T
        self.P2 = self.P0_inv @ p2

        p3 = cr.calculate_scalar_product_matrix(
            self.phi_j_dz.scale(self.psi_v_bar),
            self.phi_j).T
        self.P3 = self.P0_inv @ p3

        self.q0 = self.P0_inv @ self.phi_j_0

        q1 = cr.calculate_scalar_product_matrix(self.z_phi_N_dz, self.phi_j)
        self.q1 = self.P0_inv @ q1.squeeze()

        q2 = cr.calculate_scalar_product_matrix(self.phi_N_dz, self.phi_j_dz)
        self.q2 = self.P0_inv @ q2.squeeze()

        q3 = cr.calculate_scalar_product_matrix(
            self.phi_N_dz.scale(self.psi_v_bar),
            self.phi_j)
        self.q3 = self.P0_inv @ q3.squeeze()

        xi = cr.calculate_scalar_product_matrix(self.rho_bar, self.phi_j).T
        self.Xi = self.P0_inv @ xi

    @staticmethod
    def eval_funcs(functions, z):
        """ Helper class to quickly evaluate a set of functions """
        y_vals = []
        for func in functions:
            y_vals.append(func(z))
        y_values = np.array(y_vals)
        return y_values

    def _eval_rhs(self, t: float, xb: np.ndarray,
                  ub: Union[Callable, np.ndarray]):
        """
        Return the right hand side of the differential equation.

        Depending on the `sim_mode` provided in the constructor, either
        float values or symbolic expressions are returned.

        Args:
            t (float): Current time
            x: Current state vector.
            ub: Callable or input vector.

        Returns: Float vector of x dot.

        """
        # BIM transform is only valid in this range
        gamma = self.unscale_xb(xb)[-1]
        if gamma <= 0 or gamma >= self.len:
            logger.error("Gamma outside of model bounds, "
                         "results will not be reliable! "
                         f"Domain is (0, {self.len}) gamma is {gamma}")

        # handle inputs
        if callable(ub):
            ub = ub(t)

        # Manage error cases
        if self.heater_issues or self.heater_defect:
            if t >= 35 * 3600:
                ub = np.array([ub[0], ub[1], ub[2] + self.calibration_error,
                               self.factor_mid_heater * ub[3],
                               self.factor_through_beta_error * ub[4], ub[5]])
            else:
                ub = np.array(
                    [ub[0], ub[1], ub[2] + self.calibration_error, ub[3],
                     self.factor_through_beta_error * ub[4], ub[5]])
            n = int(t / 3600)
            if str(n) not in self.u_real_dict.keys():
                self.u_real_dict[str(n)] = ub

        x_dot = self.x_dot_fun(xb, ub).full().squeeze()
        return x_dot

    def _build_rhs(self, x: ca.SX, u: ca.SX):
        """
        Return the right hand side of the differential equation.

        Depending on the `sim_mode` provided in the constructor, either
        float values or symbolic expressions are returned.

        Args:
            x: Current state.
            u: Input vector.

        Returns: Symbolic expressions for x dot.

        """
        # disassemble state vector
        w_s = x[:self.n_fem - 1]
        w_l = x[self.n_fem - 1:-1]
        gamma = x[-1]

        # compute helpers
        beta = [gamma - bound for bound in self.orig_bounds]
        a_2 = [self.m.alpha[p_idx] / beta[p_idx] ** 2 for p_idx in range(2)]
        thetas = []
        for p_idx in range(2):
            theta_mat = ca.SX.zeros(3, 3)
            for c_idx in range(3):
                Gamma = self.orig_bounds[p_idx]
                theta_mat[0, c_idx] = (self.b_mantle[c_idx] * Gamma ** 2
                                       + self.c_mantle[c_idx] * Gamma
                                       + self.d_mantle[c_idx])
                theta_mat[1, c_idx] = beta[p_idx] * (
                        2 * self.b_mantle[c_idx] * Gamma
                        + self.c_mantle[c_idx])
                theta_mat[2, c_idx] = self.b_mantle[c_idx] * beta[p_idx]**2
            thetas.append(theta_mat)

        # top and bottom heaters (for each phase)
        u_bar = [u[p_idx] * self.deltas[p_idx] / self.m.k[p_idx] * beta[p_idx]
                 for p_idx in range(2)]

        # jacket heaters (same for both phases)
        if u.shape[0] > 2:
            u_m = u[2:5]
        else:
            u_m = np.zeros(3)
        # convert into spatially averaged form
        u_m_bar = [2 * u_m / (self.R * self.m.c[i] * self.m.rho[i])
                   for i in range(2)]

        # forced convection (for each phase)
        if u.shape[0] > 5:
            u_v = 0, u[5]
        else:
            u_v = [0] * 2

        # finally, compute the derivatives
        gamma_dot = self.calc_gamma_dot(x)
        x_dot = []
        w_tup = w_s, w_l
        for p_idx in range(2):
            w_x_dot = -a_2[p_idx] * (self.P2 @ w_tup[p_idx]
                                     + self.q2 * self.m.T_m
                                     + self.q0 * u_bar[p_idx])
            w_x_dot += gamma_dot / beta[p_idx] * (self.P1 @ w_tup[p_idx]
                                                  + self.q1 * self.m.T_m)
            w_x_dot -= u_v[p_idx] / beta[p_idx] * (self.P3 @ w_tup[p_idx]
                                                   + self.q3 * self.m.T_m)
            w_x_dot += self.Xi @ thetas[p_idx] @ u_m_bar[p_idx]
            x_dot.append(w_x_dot)

        x_dot.append(gamma_dot)
        rhs = ca.vcat(x_dot)
        return rhs

    def calc_gamma_dot(self, x: Union[np.ndarray, ca.SX]):
        """
        Compute the interface velocity.

        Args:
            x: Current state. If in sim_mode then numpy array, else casadi sx
                vector.

        Returns: Derivative of the interface velocity.

        """
        gamma = x[-1]

        # coefficients for stefan condition
        betas = gamma - 0, gamma - self.len
        s_tup = [-self.deltas[p_idx] * self.m.k[p_idx] / (
                self.m.L * self.m.rho_m * betas[p_idx])
                 for p_idx in range(2)]

        # gradients
        grad_tup = self.calc_gradients(x)

        gamma_dot = s_tup[0] * grad_tup[0] + s_tup[1] * grad_tup[1]
        return gamma_dot

    def calc_gradients(self, x: Union[np.ndarray, ca.SX]):
        """
        Compute the temperature gradients at the solid-liquid interface.

        Args:
            x: Current state. If in sim_mode then numpy array, else casadi sx
                vector.

        Returns: Tuple of (solid, liquid) gradients.

        """
        w_s = x[:self.n_fem - 1]
        w_l = x[self.n_fem - 1:-1]

        if self.sim_mode:
            tdz_s = self.phi_j_dz_1 @ w_s + self.phi_N_dz_1 * self.m.T_m
            tdz_l = self.phi_j_dz_1 @ w_l + self.phi_N_dz_1 * self.m.T_m
        else:
            tdz_s = ca.dot(self.phi_j_dz_1, w_s) + self.phi_N_dz_1 * self.m.T_m
            tdz_l = ca.dot(self.phi_j_dz_1, w_l) + self.phi_N_dz_1 * self.m.T_m

        return tdz_s, tdz_l

    def get_original_temps(self,
                           temps_s: pi.EvalData,
                           temps_l: pi.EvalData,
                           gamma_data: np.ndarray,
                           show_pg: bool = False,
                           num_z_final: int = None):
        r"""
        Performs the inverse transformation
        ..math::

            z = \bar z * \gamma_n / z = \bar z (gamma_n - l) + l

        To transform the simulation results from the fixed coordinate frame back
        into the original coordinates.

        Args:
            temps_s (:obj:`pyinduct.Evaldata`): Temps in solid domain.
            temps_l (:obj:`pyinduct.Evaldata`): Temps in fluid domain.
            gamma_data (:obj:`numpy.ndarray`): Trajectory for gamma.
            show_pg (bool): Show a plot.
            num_z_final(int) : Number of points in spatial discretization for
                T_data.

        returns:
            t_data (:obj:`pyinduct.Evaldata`): Temps in original coordinates,
            new, evenly spaced spat domain.
        """
        # transform spatial domain
        temp_domain = temps_s.input_data[0]
        spat_domain = temps_s.input_data[1]

        z_bar_steps = spat_domain.points
        z_steps = []
        for gamma_n in gamma_data:
            z_steps.append(np.hstack(
                (z_bar_steps * gamma_n,
                 z_bar_steps[:-1][::-1] * (gamma_n - self.len) + self.len)))

        # without z=l // reversed
        t_s_out = temps_s.output_data
        t_l_out_rev = temps_l.output_data[:, :-1][:, ::-1]
        output = np.hstack((t_s_out, t_l_out_rev))

        # transform time variant spatial discretization to constant
        if num_z_final is None:
            num_z_final = 2 * z_bar_steps.shape[0] - 1

        z_steps_final = np.linspace(0, self.len, num_z_final)
        output_interp = np.empty((len(temp_domain), num_z_final))

        for n in range(gamma_data.shape[0]):
            t_interp = np.interp(z_steps_final, z_steps[n], output[n])
            output_interp[n] = t_interp

        t_data = pi.EvalData([temp_domain.points, z_steps_final], output_interp)
        if show_pg:
            plots = []
            plots.append(pi.PgAnimatedPlot(t_data,
                                           labels=dict(
                                               left='T(z,t)', bottom='z'),
                                           save_pics=False,
                                           replay_gain=t_data.input_data[0][
                                                           -1] / 10))
            pi.show()
            colourplot(t_data, gamma_data)

        return t_data

    def simulate(self,
                 t_values: np.ndarray,
                 x_0: np.ndarray,
                 u_data: np.ndarray,
                 show_pg: bool = False,
                 return_state: bool = False,
                 num_z: int = None,
                 ):
        """
        Arguments:
            t_values (np.array): Simulation time grid.
            x_0 (numpy.ndarray): Initial state.
            u_data (numpy.ndarray): Input trajectory.
            show_pg (bool): Show a plot after the simulation is done.
            return_state (bool): If True, the state vector for each simulation
                time step is returned. If False Evaldata objects are returned
                (default).
            num_z (int): Number of spatial discretization points. Only relevant
                if `return_state` is false.
        Returns:
            T_s (Evaldata object):
                Temperature of the crystal
            T_l (Evaldata object):
                Temperature of the melt
            gamma_data (Evaldata object):
                Position of phase boundary

            OR

            data(numpy.ndarray):
                system state data

        """
        self.sim_mode = True
        xb_0 = self.scale_x(x_0)
        ub_data = self.scale_u(u_data)
        u_inter = interp1d(t_values, ub_data, axis=1)
        data = solve_ivp(fun=self._eval_rhs,
                         y0=xb_0,
                         t_span=(t_values[0], t_values[-1]),
                         t_eval=t_values,
                         first_step=1e-3,
                         args=(u_inter,)
                         )
        if data.status < 0:
            logger.error(data.message)
        if return_state:
            return data.y.T

        t_s, t_l, gamma_data = self.statedata_to_evaldata(data.t, data.y.T,
                                                          num_z)
        if show_pg:
            plots = []
            plots.append(pi.PgAnimatedPlot(t_s,
                                           labels=dict(
                                               left='T_s_bar(z,t)', bottom='z'),
                                           replay_gain=100))
            plots.append(pi.PgAnimatedPlot(t_l,
                                           labels=dict(
                                               left='T_l_bar(z,t)', bottom='z'),
                                           replay_gain=100))
            plt.plot(data.t / 3600, gamma_data)
            pi.show(show_mpl=True)

        temp_data = self.get_original_temps(t_s, t_l, gamma_data)

        return temp_data, gamma_data

    def post_process(self, t_grid: np.ndarray, state_data: np.ndarray):
        """
        Post processing of the raw simulation data.

        This method will extract the different variables from the state,
        transform them back into the original coordinate frame and compute
        derived variables such as gradients and velocities.

        Args:
            t_grid: Simulation time steps.
            state_data: Simulated state trajectories.

        Returns: Dict holding data for temperatures and gradients as well as the
            interface position and velocity.

        """
        ts, tl, gamma = self.statedata_to_evaldata(t_grid, state_data)
        t_data = self.get_original_temps(ts, tl, gamma)

        d_tdz_s = np.empty_like(t_grid)
        d_tdz_l = np.empty_like(t_grid)
        gamma_dot = np.empty_like(t_grid)
        for n, t in enumerate(t_grid):
            d_tdz_s_bim, d_tdz_l_bim = self.calc_gradients(state_data[n])
            d_tdz_l[n] = d_tdz_l_bim / (gamma[n] - self.len)
            d_tdz_s[n] = d_tdz_s_bim / gamma[n]
            gamma_dot[n] = self.calc_gamma_dot(state_data[n])

        return dict(temp=t_data,
                    grad_s=d_tdz_s,
                    grad_l=d_tdz_l,
                    gamma=gamma,
                    gamma_dt=gamma_dot)

    def statedata_to_evaldata(self, t_values: np.ndarray, xb_data: np.ndarray,
                              num_z: int = 200):
        """
        Convert state trajectories to eval data objects.

        Args:
            t_values: Time steps.
            xb_data: State trajectory in scaled coordinates.
            num_z: Spatial discretisation steps to use.

        Returns: Tuple of EvalData objects for solid and liquid Temperatures as
        well as interface position.

        """
        temp_domain = pi.Domain(points=t_values)
        spat_domain = pi.Domain(bounds=(0, 1), num=num_z)

        x_data = self.unscale_xb(xb_data.T).T
        w_s_data = x_data[:, :self.n_fem - 1]
        w_l_data = x_data[:, self.n_fem - 1:-1]
        gamma_data = x_data[:, -1]

        t_m_vec = np.full((gamma_data.size, 1), self.m.T_m)
        weights_s = np.hstack((w_s_data, t_m_vec))
        weights_l = np.hstack((w_l_data, t_m_vec))

        pi.register_base("vis_base", self.fem_base)
        t_s = pi.evaluate_approximation("vis_base",
                                        weights_s,
                                        temp_domain,
                                        spat_domain,
                                        name="temp_s")
        t_l = pi.evaluate_approximation("vis_base",
                                        weights_l,
                                        temp_domain,
                                        spat_domain,
                                        name="temp_l")
        pi.deregister_base("vis_base")

        return t_s, t_l, gamma_data

    def get_state(self, temp_profile: Callable, gamma: float):
        """
        Compute the system state by projecting a given temperature profile on
        the internal shape functions.

        Args:
            temp_profile: Profile to represent with the new state.
            gamma: Position of the interface velocity.

        Returns: np.ndarray holding the compatible system state.

        """
        w_dim = self.n_fem - 1
        x = np.zeros((2 * w_dim,))

        def _coord_map(_idx):
            def zb_wrapper(zb):
                zm = self.orig_bounds[_idx]
                z = zb * (gamma - zm) + zm
                return temp_profile(z)
            return zb_wrapper

        for idx in range(2):
            # build eval handle
            zb_func = pi.Function(eval_handle=_coord_map(idx),
                                  nonzero=(0, 1),
                                  domain=(0, 1))
            # pi.visualize_functions([zb_func])

            if 1:
                w = pi.project_on_base(zb_func, self.fem_base)[:-1]
            else:
                # for exact starting values, use collocation and skip w_N
                w = zb_func(self.nodes[:-1])

            # sort values into weight array
            x[idx * w_dim: (idx + 1) * w_dim] = w

        return np.hstack((x, gamma))

    def get_state_map(self, other):
        """
        Compute a state mapping from this model to an `other` one.

        This is useful if the state of a more precise model shall be used in a
        controller with a less precise internal model and must therefore be
        mapped.

        Note:
            This only works between different FE-Models as they know how their
            state vectors are composited.

        Args:
            other: Model to map the state to.

        Returns: Callable state mapping that will convert the state of this
        model into the state of the given model.

        """
        # try parent first
        m = super().get_state_map(other)
        if m != NotImplemented:
            return m

        # so far we only know about 1d fem models
        if not isinstance(other, FEModel):
            return NotImplemented

        # check state dimension
        if self.n_fem == other.n_fem:
            def _scale_map(_xb):
                return other.scale_x(self.unscale_xb(_xb))
            return _scale_map

        src_base = self.fem_base
        dst_base = other.fem_base
        trafo_mat = pi.calculate_base_transformation_matrix(src_base,
                                                            dst_base)

        def _state_map(src_xb):
            src_x = self.unscale_xb(src_xb)
            dst_x = np.nan * np.ones(other.n_x)
            src_offset = self.n_fem - 1
            dst_offset = other.n_fem - 1
            for p in range(2):
                # select reduced state vector of src phase
                xp_rs = src_x[p*src_offset:(p+1)*src_offset]
                # append the fixed last weight
                xp_fs = np.append(xp_rs, self.m.T_m)
                # transform into other state space
                xp_fd = trafo_mat @ xp_fs
                # remove the fixed last weight
                xp_rd = xp_fd[:-1]
                # write back into reduced state vector of dst phase
                dst_x[p*dst_offset:(p+1)*dst_offset] = xp_rd

            # copy the interface position
            dst_x[-1] = src_x[-1]

            return other.scale_x(dst_x)

        return _state_map
