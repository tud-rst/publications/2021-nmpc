import numpy as np
from model_1d import Material

from analytic_solution import ground_truth, get_neumann_boundaries


def test_ground_truth():
    t_s = 1501.15
    t_l = 1521.15
    mat = Material("GaAs_const_rho")
    t_vals = np.linspace(0, 20 * 3600, 1000)
    ground_truth(t_s, t_l, mat, dom_len=0.1, t_values=t_vals, show_plot=False)


def test_set_neumann_rb():
    t_s = 1501.15
    t_l = 1521.15
    mat = Material("GaAs_const_rho")
    t_vals = np.linspace(0, 20 * 3600, 1000)
    get_neumann_boundaries(t_s, t_l, mat, 1, t_vals)


if __name__ == "__main__":
    # Run Tests
    test_ground_truth()
    test_set_neumann_rb()
