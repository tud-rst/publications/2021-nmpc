import logging
import os
import pickle
import numpy as np
import casadi as ca
from mpctoolbox import (
    OptimalControlProblem, OCPMode,
    MPCMode, MPController,
    InitialValueProblem, Simulator,
)
import pyinduct as pi

from stefan.model_1d import Material, FEModel

logger = logging.getLogger(__name__)


class VGFProcess:
    """
    Class that encapsulates the Vertical Gradient Freeze crystal growth process
    of a Gallium-Arsenide single crystal.

    While the available parameters allow to simulate different control
    scenarios, the most values are hard-coded as they arise from practical
    considerations.

    Args:
        ocp_mode (OCPMode): End constraint formulation of the optimal control
            problem (OCP) to be generated. See `mpctoolbox.OCPMode` for details.
        number_of_inputs (int): Number of system inputs to simulate. Available
            choices are:
                * 2: Only top and bottom heaters
                * 5: Top, bottom and three jacket heaters
                * 6: All heaters and the forced melt convection
        n_fem (int): Number of finite elements to use for the approximation of
            the reference model, used in the controller.
        dom_len (float): Length of the spatial domain to simulate. In general
            the length of the crucible used in the growth process and thus the
            maximum crystal length.
        t_end (float): Time stamp (in seconds) when the growth process shall be
            completed.
        t_step (float): Step width(in seconds) to use for the discretisation of
            the system. This will only affect the steps used by the controller.
            The simulator will use automatic step size control to ensure proper
            simulation errors.
        mpc_mode (MPCMode): Operation mode of the model predictive controller.
            See `mpctoolbox.MPCMode` for details.
        N (int): Length of receding optimization horizon used in the controller.
            The resulting horizon time span is given by `N` times `t_step`.
        param_deviation (bool): If True, the controller reference  and the
            actual simulation system will have a parameter deviation of 10%.
        heater_defect (bool): If True, a defect of the middle heater is
            simulated at t=35 hours.
        heater_issues (bool): If True, simulate a fixed offset (100 W/m^2) at
            the bottom mantle heater and allow only 90% of heating/cooling
            effects of top mantle heater.

    Note:
        Only a small subset of `ocp_mode` and `mpc_mode` combinations do make
        sense. Please consult their definitions in case of problems.

    """
    def __init__(self,
                 ocp_mode: OCPMode,
                 number_of_inputs: int,
                 n_fem: int,
                 dom_len: float,
                 t_end: float,
                 t_step: float,
                 mpc_mode: MPCMode,
                 N: int,
                 n_fem_sim: int = None,
                 initial_error: bool = False,
                 param_deviation: bool = False,
                 heater_issues: bool = False,
                 heater_defect: bool = False
                 ):
        # MPC setup
        self.N = N
        self.ocp_mode = ocp_mode
        self.mpc_mode = mpc_mode

        self.t_grid = np.linspace(0, t_end, int(t_end / t_step) + 1)
        self.t_grid_sim = np.linspace(0, t_end, 10 * len(self.t_grid))
        self.t_step = t_step

        self.x_guess = None
        self.u_guess_complete = None
        self.u_guess = None

        self.u_prev = None
        self.u_ref = None
        self.u_end = None

        self.x_0 = None
        self.x_ref = None
        self.x_end = None

        self.ocp = None
        self.cont = None
        self.ivp = None

        self.cost_functions = None
        self.sol = None

        self.suffix = ""

        self.number_of_inputs = number_of_inputs

        # Process disturbances
        self.param_deviation = param_deviation
        self.heater_issues = heater_issues
        self.heater_defect = heater_defect

        # Model Parameters
        self.n_fem_sim = n_fem_sim
        self.dom_len = dom_len

        # Design Model
        self.n_fem_des = n_fem
        self.mat_des = Material("GaAs")
        self.sys_des = self.create_system(self.n_fem_des,
                                          self.dom_len,
                                          self.mat_des,
                                          scale_model=True)

        # initial conditions
        grad_s_start = 10 * 10  # [] = K / m
        grad_l_start = grad_s_start * self.mat_des.k[0] / self.mat_des.k[
            1]  # [] = K / m
        gamma_start = 20 / 1000  # [] = m
        u_prev = np.array([-2e3, 1e3,
                           0, 0, 0,
                           0])
        self.set_start_conditions(grad_s_start,
                                  grad_l_start,
                                  gamma_start,
                                  u_prev)

        # targets (used in fixed endpoint OCP formulations)
        grad_s_end = 1 * 10  # [] = K / m
        grad_l_end = grad_s_end * self.mat_des.k[0] / self.mat_des.k[
            1]  # [] = K / m
        gamma_end = 380 / 1000  # [] = m
        u_end = np.array([-100 * self.mat_des.k[0], 100 * self.mat_des.k[1],
                          10, 10, 10,  # [] = W/m^2
                          10 / (1000 * 3600),  # [] = m/s
                          ])
        self.set_end_conditions(grad_s_end,
                                grad_l_end,
                                gamma_end,
                                u_end)

        # references
        self.vi_ref = 5 / (1000 * 3600)  # [] = m/s
        self.grad_s_ref = 100 * 10  # [] = K / m
        self.grad_l_ref = 1 / self.mat_des.k[1] * (
                self.grad_s_ref * self.mat_des.k[0]
                - self.vi_ref * self.mat_des.rho_m * self.mat_des.L
        )
        self.set_ref_conditions(self.grad_s_ref,
                                self.grad_l_ref,
                                self.dom_len/2)

        # Quality goals
        self.vi_min = -0.1 / (1000 * 3600)  # [] = m/s
        self.vi_max = 7 / (1000 * 3600)  # [] = m/s
        self.grad_l_min = 2 * 10  # [] = K/m
        self.vg_max = 6 * 1000 / 3600 * 1000  # [] = m**2 / (K s)

        # Build control objects
        self.build_ocp()
        # self.build_guess()
        self.build_cont()

        # Simulation System
        if n_fem_sim is not None:
            self.n_fem_sim = n_fem_sim
        else:
            self.n_fem_sim = self.n_fem_des
        self.mat_sim = Material(
            "GaAs",
            uncertainty_factor=1.1 if param_deviation else 1)
        self.sys_sim = self.create_system(self.n_fem_sim,
                                          self.dom_len,
                                          self.mat_sim,
                                          self.heater_issues,
                                          self.heater_defect)

        # simulation initial state
        self.initial_error = initial_error
        if initial_error:
            grad_s_sim = 17e3
            zi_sim = 1e-3
            vi_sim = - 1 / (1000 * 3600)
            grad_l_sim = 1 / self.mat_sim.k[1] * (
                    grad_s_sim * self.mat_sim.k[0]
                    - vi_sim * self.mat_sim.rho_m * self.mat_sim.L
            )
            self.x_0_sim = self.get_state_from_eq_cond(self.sys_sim,
                                                       grad_s_sim,
                                                       grad_l_sim,
                                                       zi_sim)
        else:
            self.x_0_sim = self.get_state_from_eq_cond(self.sys_sim,
                                                       grad_s_start,
                                                       grad_l_start,
                                                       gamma_start)
        self.build_ivp()

    def build_ocp(self):
        """
        Construct the underlying optimal control problem (OCP) which is to be
        solved by the controller.
        """
        constraint_dict = self.get_constraints()
        cost_dict = self.get_costs()
        ocp = OptimalControlProblem(self.sys_des,
                                    self.ocp_mode,
                                    self.x_0,
                                    self.t_grid,
                                    self.x_end,
                                    self.u_end,
                                    u_prev=self.u_prev,
                                    **constraint_dict,
                                    **cost_dict,
                                    )
        self.ocp = ocp

    def build_ivp(self):
        """
        Construct the InitialValueProblem (IVP) which is to be solved by means
        of numerical integration.
        """
        ivp = InitialValueProblem(self.sys_sim, self.x_0_sim, self.t_grid_sim)
        self.ivp = ivp

    def build_guess(self):
        """
        Construct a sane initial guess for the optimiser.
        """
        if self.mpc_mode == MPCMode.OPEN_LOOP:
            hor_len = len(self.ocp.t_grid)
        else:
            hor_len = self.N + 1

        # state
        rnd_values = np.random.random_sample((self.sys_des.n_fem - 1, hor_len))
        xg = self.mat_des.T_m * np.ones((self.sys_des.n_x, hor_len))
        xg[:self.n_fem_des - 1] -= 100 * rnd_values     # solid
        xg[self.n_fem_des - 1:-1] += 100 * rnd_values   # liquid
        xg[-1] = .9 * self.dom_len * rnd_values[0]      # gamma
        self.x_guess = xg

        # input
        ug = (np.random.random_sample((6, hor_len - 1)) - 0.5)
        ug[:2] *= 1e2   # top and bottom heaters
        ug[2:5] *= 1e1  # jacket heaters
        ug[5] *= 1e-5   # melt convection
        self.u_guess_complete = ug
        self.u_guess = self.u_guess_complete[:self.number_of_inputs]

    def build_cont(self):
        """
        Construct the model predictive controller
        """
        solver_kwargs = None
        if self.mpc_mode == MPCMode.OPEN_LOOP:
            solver_kwargs = dict(time_limit=24*60*60,
                                 max_iter=1e5)

        cont = MPController(self.ocp, self.mpc_mode, self.N,
                            self.x_guess, self.u_guess,
                            solver_kwargs=solver_kwargs,
                            verbosity=0,
                            # show_step=True,
                            # allow_relaxation=True,
                            continue_on_failure=True,
                            )
        self.cont = cont

    def simulate(self):
        """
        Simulate the closed loop system
        """
        simulator = Simulator(self.ivp, self.cont)
        self.sol = simulator.solve()

    def post_process(self):
        """
        Run post processing on simulation results
        """
        # collect data from simulation model
        res = self.ivp.sys.post_process(self.sol["t_grid"],
                                        self.sol["x_sim"])
        self.sol.update(res)

    def create_system(self, n_fem: int, dom_len: float, material: Material,
                      heater_issues: bool = False, heater_defect: bool = False,
                      scale_model: bool = False):
        """
        Construct a finite-element approximation of the VGF Process

        Args:
            n_fem (int): Number of FE-elements to use.
            dom_len (float): Length of the simulation domain.
            material (Material): Material to simulate.
            heater_issues (bool): If True, simulate heater problems.
            heater_defect (bool): If True, simulate heater defects.
            scale_model (bool): If True, scale model state.

        Note:
            The model scaling is used to improve the convergence of the numeric
            solution.

        Returns: FEModel, the process model.

        """
        dx = np.ones(FEModel.get_x_dim(n_fem))
        du = np.ones(self.number_of_inputs)
        if scale_model:
            # states
            dx[:-1] /= material.T_m   # scale temps by melting temperature
            dx[-1] /= 1               # leave gamma as it is

            if 1:
                # inputs
                du[:2] /= 1e4  # top and bottom heat flows
                if len(du) > 2:
                    du[2:5] /= 1e3  # jacket heat flows
                if len(du) > 5:
                    du[5] /= 1e-5  # forced convection
        sys = FEModel(n_fem, dom_len, material, dx, du,
                      u_dim=self.number_of_inputs,
                      heater_issues=heater_issues,
                      heater_defect=heater_defect)
        return sys

    def set_ref_conditions(self, grad_s_ref: float, grad_l_ref: float,
                           gamma_ref: float, u_ref: np.ndarray = None):
        """
        Construct the reference state and input used in the stage costs
        throughout the process.

        Args:
            grad_s_ref (float): Solid gradiant at the interface.
            grad_l_ref (float): Liquid gradient at the interface.
            gamma_ref (float): Solid/Liquid interface position.
            u_ref (np.ndarray): Reference inputs, defaults to Zero.

        """
        self.x_ref = self.get_state_from_eq_cond(self.sys_des,
                                                 grad_s_ref,
                                                 grad_l_ref,
                                                 gamma_ref,
                                                 )
        self.x_ref[-1] = gamma_ref
        if u_ref is None:
            u_ref = np.zeros(self.sys_des.n_u)
        self.u_ref = u_ref

    def set_start_conditions(self, grad_s_start: float, grad_l_start: float,
                             gamma_start: float, u_prev: np.ndarray):
        """
        Construct the initial state and input used in the simulation.

        Args:
            grad_s_start (float): Solid gradiant at the interface.
            grad_l_start (float): Liquid gradient at the interface.
            gamma_start (float): Solid/Liquid interface position.
            u_prev (np.ndarray): Previous input values (needed for derivative
                computation).

        """
        self.x_0 = self.get_state_from_eq_cond(self.sys_des,
                                               grad_s_start,
                                               grad_l_start,
                                               gamma_start)
        self.u_prev = u_prev[:self.number_of_inputs]

    def set_end_conditions(self, grad_s_end: float, grad_l_end: float,
                           gamma_end: float, u_end: np.ndarray):
        """
        Construct the final state and input used in the planning.

        Args:
            grad_s_end (float): Solid gradiant at the interface.
            grad_l_end (float): Liquid gradient at the interface.
            gamma_end (float): Solid/Liquid interface position.
            u_end (np.ndarray): Final input values.
        """
        self.x_end = self.get_state_from_eq_cond(self.sys_des,
                                                 grad_s_end,
                                                 grad_l_end,
                                                 gamma_end)
        self.u_end = u_end

    def get_state_from_eq_cond(self, sys: FEModel, grad_s: float, grad_l: float,
                               gamma: float):
        """
        Convert a piecewise linear temperature profile into a state vector
        of an FE-Approximation.

        Args:
            sys (FEModel): Model for which the state vector is to be computed.
            grad_s(float): Solid gradiant at the interface.
            grad_l(float): Liquid gradient at the interface.
            gamma (float): Solid/Liquid interface position.

        Returns: np.ndarray, the respective system state for the given model.
        """
        # get profile
        temp_profile, grad_profile = self.build_linear_temp_func(grad_s,
                                                                 grad_l,
                                                                 gamma)

        if 0:
            # visualise the generated profile
            z_func = pi.Function(eval_handle=temp_profile,
                                 derivative_handles=[grad_profile],
                                 nonzero=(0, sys.len),
                                 domain=(0, sys.len))
            pi.visualize_functions([z_func, z_func.derive(1)])

        # get state
        x_eq = sys.get_state(temp_profile, gamma)

        if 0:
            # check if the generated state actually represents the gradients
            gs, gl = sys.calc_gradients(x_eq)
            tdz_s = gs / (x_eq[-1] - 0)
            tdz_l = gl / (x_eq[-1] - sys.len)
            logger.info(sys.calc_gamma_dot(x_eq))
            logger.info((tdz_s, tdz_l))

        return x_eq

    def build_linear_temp_func(self, grad_s: float, grad_l: float,
                               gamma: float):
        """
        Construct a linear (equilibrium) temperature profile lookup table.

        Args:
            grad_s(float): Solid gradiant at the interface.
            grad_l(float): Liquid gradient at the interface.
            gamma (float): Solid/Liquid interface position.

        Returns: Callable lookup table.
        """

        def temp_func(z):
            """ A linear temperature profile """
            z = np.atleast_1d(z)
            r = np.ones_like(z) * self.mat_des.T_m
            dz = z - gamma
            m = dz >= 0
            r[~m] += grad_s * dz[~m]  # solid
            r[m] += grad_l * dz[m]  # liquid
            if z.size == 1:
                return float(r)
            return r.squeeze()

        def temp_func_dz(z):
            """ A constant temperature gradiant """
            z = np.atleast_1d(z)
            r = np.zeros_like(z)
            dz = z - gamma
            m = dz >= 0
            r[~m] += grad_s  # solid
            r[m] += grad_l   # liquid
            if z.size == 1:
                return float(r)
            return r.squeeze()

        return temp_func, temp_func_dz

    def get_costs(self):
        """
        Construct the cost functions for the OCP.

        Note:
            The costs are evaluated with the scaled system state und thus
            defined this way.
        """
        sys = self.sys_des
        xb = sys.x
        ub = sys.u
        xb_sp = ca.SX.sym('x_sp', xb.shape[0])
        ub_sp = ca.SX.sym('u_sp', ub.shape[0])
        e_x = self.sys_des.scale_x(xb - xb_sp)
        e_u = self.sys_des.scale_u(ub - ub_sp)

        # basic costs
        q = np.ones(sys.n_x)
        q[-1] *= sys.n_x - 1  # level the influence of gamma against temps
        q_mat = np.diag(q)

        r = np.ones(6)
        r[-1] /= 1e2  # forced convection is much cheaper than heating
        if self.number_of_inputs == 2:
            r_mat = np.diag(r[:2])
        elif self.number_of_inputs == 5:
            r_mat = np.diag(r[:5])
        else:
            r_mat = np.diag(r)

        l_state = (e_x.T @ q_mat @ e_x + e_u.T @ r_mat @ e_u)

        grad_s, grad_l, gamma_dot = self._get_sym_expressions(sys, xb)

        v_over_g = gamma_dot / grad_s

        # process targets
        shift = 2
        factor = q[-1]
        m = 10
        mv = m * 1000 * 3600
        mg = m * 10
        mvg = m * 1e4

        l_gamma_dot = (0
                       + 1 + ca.erf(-mv * (gamma_dot - self.vi_min) - shift)
                       + 1 + ca.erf(mv * (gamma_dot - self.vi_max) + shift)
                       )
        l_grad_l = 1 + ca.erf(-mg * (grad_l - self.grad_l_min) - shift)
        l_vg = 1 + ca.erf(mvg * (v_over_g - self.vg_max) - shift)
        l_expr = l_state + factor * (0
                                     + l_gamma_dot
                                     + l_grad_l
                                     + l_vg
                                     )

        # store expressions for later analysis
        self.cost_functions = {"L_gamma": l_state,
                               "L_vg": l_vg,
                               "L_gamma_dot": l_gamma_dot,
                               "L_dTdz_gamma": l_grad_l,
                               "l_complete": l_expr}

        # build casadi function
        l_func = ca.Function("L_func",
                             [xb, ub, xb_sp, ub_sp],
                             [l_expr],
                             ["x", "u", "x_sp", "u_sp"],
                             ("L",))

        # final costs
        lf_func = None
        # lf_func = 1e3 * l_state

        cost_dict = dict(l_func=l_func, lf_func=lf_func)

        return cost_dict

    def _get_sym_expressions(self, sys, xb):
        """
        Helper function that constructs symbolic expression for the solid and
        liquid side gradients at the interface as well as for the interface
        velocity.

        The expression are simply taken from the given model and then
        transformed back into the original fixed coordinate frame.

        Args:
            sys (FEModel): Simulation model
            xb (cs.SX): System state as symbolic vector.

        Returns: Tuple of expressions as casadi SX vectors.

        """
        # the constraint functions will be called with the scaled state
        x = self.sys_des.unscale_xb(xb)
        # symbolic expressions for quality properties
        gamma_dot = sys.calc_gamma_dot(x)
        tdz_s_bim, tdz_l_bim = sys.calc_gradients(x)

        # convert bim transformed gradient to original system
        tdz_s = tdz_s_bim / (x[-1] - 0)
        tdz_l = tdz_l_bim / (x[-1] - self.dom_len)

        return tdz_s, tdz_l, gamma_dot

    def get_constraints(self):
        """
        Construct the constraints for the VGF process.

        These include the constraints for:
            * State variables
            * Input variables
            * Input derivatives
            * Gradients at the interface
        """
        n_fem = self.n_fem_des
        eps = 0.00
        temp_interval = 250  # K

        w_dim = n_fem - 1
        x_s_min = np.full(w_dim, self.mat_des.T_m - temp_interval)
        x_l_min = np.full(w_dim, self.mat_des.T_m + eps)
        g_min = 1e-3
        x_min = np.hstack((x_s_min, x_l_min, g_min))
        lb = dict(x=x_min)

        x_s_max = np.full(w_dim, self.mat_des.T_m - eps)
        x_l_max = np.full(w_dim, self.mat_des.T_m + temp_interval)
        g_max = self.dom_len - 1e-3
        x_max = np.hstack((x_s_max, x_l_max, g_max))
        ub = dict(x=x_max)

        if self.number_of_inputs == 2:
            u_c = np.array([15000, 15000])
            ud_c = np.array([1000, 1000])
        elif self.number_of_inputs == 5:
            u_c = np.array([15000, 15000, 1500, 1500, 1500])
            ud_c = np.array([1000, 1000, 100, 100, 100])
        elif self.number_of_inputs == 6:
            u_c = np.array([15000, 15000, 1500, 1500, 1500,  # [] = W
                            10 / (1000 * 3600)  # [] = m/s
                            ])
            ud_c = np.array([.5, .5, .05, .05, .05,  # [] = W/s
                             1 / (1000 * 3600**2)  # [] = m/s**2
                             ]) * self.t_step
        else:
            raise NotImplementedError()

        lb["u"] = -u_c
        ub["u"] = u_c
        lb["Du"] = -ud_c
        ub["Du"] = ud_c

        # state-dependent constraints
        eps = 1e-7
        x = self.sys_des.x
        grad_s, grad_l, gamma_dot = self._get_sym_expressions(self.sys_des, x)

        # growth rate
        c_gdt_min = (self.vi_min - gamma_dot) / np.abs(self.vi_min) + eps
        c_gdt_max = (gamma_dot - self.vi_max) / np.abs(self.vi_max) + eps

        # gradients
        c_gs = (0 - grad_s) / self.grad_l_min  # grad_s > 0
        c_gl = (0 - grad_l) / self.grad_l_min  # grad_l > 0

        e_expr = ca.vcat([
            c_gdt_min,
            c_gdt_max,
            c_gs,
            c_gl,
        ])
        e_func = ca.Function("e_func", [x], [e_expr], ["x"], ["e"])

        constraint_dict = dict(lb=lb, ub=ub,
                               # e_func=e_func
                               )

        return constraint_dict

    def save_data(self, file_name: str = None):
        """
        Export the simulation results.

        Args:
            file_name (str): Path to the result file. If not provided, defaults
                to a canonical name constructed by the chosen parameters.

        """
        t_step = self.t_grid[1] - self.t_grid[0]
        params = dict(domain=(0, self.dom_len),
                      N=self.n_fem_des,
                      t_step=t_step,
                      t_grid=self.t_grid,
                      x_0=self.x_0,
                      N_sim=self.n_fem_sim,
                      param_deviation=self.param_deviation,
                      heater_issues=self.heater_issues,
                      heater_defect=self.heater_defect)
        res = {**params, **self.sol}

        if file_name is None:
            file_name = f"{self.ocp_mode}" \
                        f"_{self.mpc_mode}" \
                        f"_u{self.number_of_inputs}" \
                        f"_K{self.n_fem_des}" \
                        f"_Ksim{self.n_fem_sim}"

        f_path = os.path.join(os.pardir, "data", f"{file_name}.pkl")
        logger.info(f"Writing results to {f_path}")
        with open(f_path, "wb") as f:
            pickle.dump(res, f)

        return file_name
