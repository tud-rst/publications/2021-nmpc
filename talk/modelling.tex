%\setbeamertemplate{tud background}[image/shaded]{crystal_purple.pdf}{0.8}
\toclesssection{Modelling}

\begin{frame}	
	\begin{columns}
		\column{.6\textwidth}
			\begin{figure}[t]
				\center
				\resizebox{\linewidth}{!}{\input{../include/system_sketch_1}}
			\end{figure}
		\column{.4\textwidth}
			\begin{block}{\bfseries Assumptions}
				\begin{itemize}	
					\item<1-> Cylindrical crucible
					\item<2-> Heaters at crucible outsides
					\item<5-> Rotational symmetry
					\item<6-> Natural convection negligible
				\end{itemize}
			\end{block}	
			\begin{block}<7->{\bfseries Result}
				\begin{itemize}	
					\item<8-> Two coupled 2d heat diffusion problems
					\item<9-> Separated by the interface
				\end{itemize}
			\end{block}
	\end{columns}
\end{frame}

\begin{frame}
	% TODO use columns and show rectangle on the right
	%\begin{block}
	\textbf{Two-dimensional Temperature distribution in each phase:}\\
	\textcolor<2->{HKS57K100}{Diffusion}-\textcolor<3->{HKS36K100}{Advection}-Equation 
	for $\tfreec$ in (planar) cylindrical coordinates
		\begin{align*}
			\partial_t\tfreec &=
			\color<2->{HKS57K100}{
				\hd\left(
					\tfrac{1}{r}\partial_r\left(r\partial_r\tfreec\right)
					+ \partial^2_z\tfreec
				\right)
			}
			\\ &\quad
			\color<3->{HKS36K100}{
				- \vr \partial_r\tfreec
			}
			\\ &\quad
			\color<3->{HKS36K100}{
				- \vz \partial_z\tfreec
			}
		\end{align*}

	\onslide<4->{
		{\bfseries Boundary conditions:}
		\begin{align*}
			& \text{Top/Bottom heater:}	&\hc\partial_z \tfree(r, \zb, t) &= \bfd q(r,t)\\
			& \text{Jacket heaters:}		&\hc\partial_r \tfree(R, z, t) &= \qm\\
			& \text{Inner boundary:}		&\partial_r \tfree(0, z, t) &= 0\\
			& \text{Interface:}	&\tfree(r,\zic, t) &= \tm
		\end{align*}%
	}

	\onslide<1->{
		{\bfseries Symbols:}
	}
	{\footnotesize
		\onslide<1->{
			$\partial_{x}\dotsc$Partial derivative wrt.~$x$, 
			$\hd\dotsc$Heat diffusivity, 
			$\hc\dotsc$Heat conductivity, 
			$\vr,\vz\dotsc$Vortex velocities, 
		}
		\onslide<4->{
			$q,\qm\dotsc$Heat flows, 
			$\tm\dotsc$Melting Temperature, 
			$\bfd\dotsc$Flow direction ($-1$ in crystal, $1$ in melt), 
			$R\dotsc$Crucible radius, 
			$\zb\dotsc$Top/Bottom crucible boundary
		}
	}
\end{frame}

\begin{frame}%{Input modelling}
	\begin{columns}[t]
		\column{.5\textwidth}
			\pause
			\begin{block}{\bfseries Jacket Heaters}
				\pause
				\begin{itemize}
					\item Modelled as scalar inputs with spatial characteristics
				\end{itemize}
				\pause
				\begin{equation*}
					\qm = \sum_{i=1}^{3} \psi_i(z) \inpm{i} = \psiv \inpmv
				\end{equation*}
			\end{block}			
			\pause
			\begin{figure}
			   \center
			   \includegraphics[width=.9\textwidth]{../include/jacket_characteristics.pdf}
			\end{figure}

		\column{.5\textwidth}	
			\pause
			\begin{block}{\bfseries Forced convection terms}
				\pause
				\begin{itemize}
					\item Modelled as fixed vortexes that are scaled by the input
				\end{itemize}%
				\pause
				\begin{equation*}
					\begin{aligned}
						\vz &= \chi_{\mathrm{v}}(r, z)\inpbc\\
						\vr &= \chi_{\mathrm{r}}(r, z)\inpbc
					%\\\bs{\chi}(r,z) &= \left(\chi_{\mathrm{v}}(r, z), \chi_{\mathrm{v}}(r, z)\right)
					\end{aligned}%
				\end{equation*}
			\end{block}	
			\pause
			\begin{figure}
			   \center
			   \includegraphics[width=.9\textwidth]{../include/vortex_characteristics.pdf}
			\end{figure}
	\end{columns}	
\end{frame}

\begin{frame}%{Reduction to 1d problem}
	\begin{columns}[t]
		\column{.5\textwidth}
			\pause
			\begin{block}{\bfseries Assumptions}
				\begin{itemize}
					\pause
					\item Small radial   temperature gradient
					\pause
					\item Component $\vr$ has zero mean %$\int\nolimits_0^R\vr\dop r = 0$
				\end{itemize}
			\end{block}		
			\pause
			\begin{block}{\bfseries Radially-averaged expressions}
				\pause
				\begin{equation*}
					\begin{aligned}
						\torigz &\coloneqq \frac{2}{R^2}\int_0^R \tfreec r \dop r\\
						\pause
						\psi_{\mathrm{v}}(z) &\coloneqq \frac{1}{R}\int_0^R \chi_{\mathrm{v}}(r,z) \dop r\\
						\inp &\coloneqq \frac{1}{R} \int_0^R q(r,t) \dop r\\
						\inpbmv &\coloneqq \inpmv\,2/(R\dn\stc)
					\end{aligned}	
				\end{equation*}
			\end{block}	

		\column{.5\textwidth}
			\pause
			\textbf{One-dimensional dynamics:}%(each phase)	
			\begin{align*}
				\partial_t \torigz &= \color{HKS57K100}{\hd\partial_z^2 \torigz}
					\\ &\quad
					\color{HKS36K100}{-\psi_{\mathrm{v}}(z)\inpbc \partial_z \torigz}
					\\ &\quad
					\color{HKS07K100}{+\psiv\inpbmv}
			\end{align*}
			\pause
			with \textbf{Boundaries:}	
			\begin{align*}
				\hc\partial_z \torig(\zb, t) &= \bfd\inp\\
				\torig(\zi, t) &= \tm
			\end{align*}	
			\pause
			coupled by \textbf{Stefan Condition:}
			\begin{equation*}
				\dmelt L\vi = \hcs\partial_z\ts(\zi, t) - \hcl\partial_z\tl(\zi, t)
			\end{equation*}
			\pause
			\ding{212} 2 PDES with free boundary coupled by an ODE:
			\textbf{``Two-phase Stefan Problem''}
	\end{columns}	
\end{frame}

